var TrainYourBrain = TrainYourBrain || {};
TrainYourBrain.Kopfrechnen = (function (){
    "use strict";
    /* eslint-env browser */
    var that = new EventPublisher(),
        spielKr,
        timerKr,
        startKr,
        aufgabeKr,
        eingabeKr,
        bestätigenKr,
        zurückKr,
        ergebnis,
        gameRunning = false;
    
    const TIME = 60;
    
        
    
    function init() {
        
        spielKr = document.getElementById("spielKopfrechnen");
        timerKr = document.getElementById("timerKopfrechnen");
        updateTimer(TIME);
        startKr = document.getElementById("startSpielKopfrechnen");
        startKr.addEventListener("click",startSpiel);
        aufgabeKr = document.getElementById("rechenaufgabe");
        eingabeKr = document.getElementById("kopfrechnenInput");
        bestätigenKr = document.getElementById("kopfrechnenBestätigen");
        bestätigenKr.addEventListener("click", bestätigeEingabe);
        zurückKr = document.getElementById("zurückKopfrechnen");
        zurückKr.addEventListener("click", zurückZumMenü);
       
    }
    
    function start(){
        
        spielKr.classList.remove("hidden"); 
        
    }
    
    
    function startSpiel() {
        if(!gameRunning){
            aufgabeKr.innerHTML = getTask();
            gameRunning = true;
            countdown(TIME);
        }
    }
    
    function updateTimer(time){
        var timeView = "",
            minutes,
            seconds;
        minutes = Math.floor(time/60);
        seconds = time - minutes*60;
        
        if(seconds < 10) {
            timeView = minutes + ":0" + seconds;
        } else{
            timeView = minutes + ":" + seconds;
        }
        
        timerKr.innerHTML = timeView;
    }
    
    
    function countdown(time) {
        var timeout;
        if(gameRunning){
            updateTimer(time);
            if(time > 0) {
                timeout = window.setTimeout(countdown, 1000, time-1);
            } else{
                gameRunning = false;
            }
        }
    }
    

    
    function bestätigeEingabe(){
        
        if(eingabeKr.value == ergebnis){
            
            aufgabeKr.innerHTML = getTask();
            eingabeKr.value = "";
            console.log("richtig");
            
        } else {
            
            eingabeKr.value = "";
            console.log("falsch");
            
        }
        
    }
    
    function zurückZumMenü() {
        if(gameRunning){
            gameRunning = false;
        }
        
        updateTimer(TIME);
        spielKr.classList.add("hidden");
        that.notifyAll("zumMenü");
    }
    
    function getTask(){
        
        const PLUS = 0;
        const MINUS = 1;
        const MAL = 2;
        
        const MIN = 1;
        const MAX_STRICH = 1000;
        const MAX_PUNKT = 20;
        
        var task = "",
            num1,
            num2;
        
        switch(Math.floor((Math.random() *3) +0)){
        case PLUS:
            num1 = Math.floor((Math.random() *MAX_STRICH) + MIN);
            num2 = Math.floor((Math.random() *MAX_STRICH) + MIN);
            ergebnis = num1 + num2;
            task = num1 + "+" + num2 + "= ?";
            return task;
        case MINUS:
            num1 = Math.floor((Math.random() *MAX_STRICH) + MIN);
            num2 = Math.floor((Math.random() *MAX_STRICH) + MIN);
            ergebnis = num1 - num2;
            task = num1 + "-" + num2 + "= ?";
            return task;
        case MAL: 
            num1 = Math.floor((Math.random() *MAX_PUNKT) + MIN);
            num2 = Math.floor((Math.random() *MAX_PUNKT) + MIN);
            ergebnis = num1 * num2;
            task = num1 + "*" + num2 + "= ?";
            return task;
        default:
            break;
        }
        
        
    }
    
    that.init = init;
    that.start = start;
    return that;
}());