var TrainYourBrain = TrainYourBrain || {};
TrainYourBrain.AlphabetundZahlen = (function (){
    "use strict";
    /* eslint-env browser */
    var that = new EventPublisher(),
        spielAlphabetZahlen,
        timer,
        startButton,
        task,
        zurückButton,
        currentTask,
        gameRunning = false,
        taskTimeout;
    
    const CLOSER_A = "ABCDEFGHIJKLM";
    const CLOSER_Z = "NOPQRSTUVWXYZ";
    const LETTER_A = 0;
    const LETTER_Z = 1;
    const NUMBER = 2;
    const KEYCODE_Y = 89;
    const KEYCODE_X = 88;
    const KEYCODE_N = 78;
    const KEYCODE_M = 77;
    const TIME = 120;
    const TASK_TIMER = 2000;
    
    function onRightAnswer() {
        console.log("richtig");
        clearTimeout(taskTimeout);
        getTask();
    }
    
    function onWrongAnswer() {
        console.log("falsch");
        clearTimeout(taskTimeout);
        getTask();
    }
    
    function onNewTask() {
        if(gameRunning){
            taskTimeout = window.setTimeout(getTask, TASK_TIMER);
        }
    }
    
    function getTask() {
        var random = Math.floor((Math.random() *4) +0);
        if(random > 1) {
            currentTask = NUMBER;
            task.innerHTML = Math.floor((Math.random() *10) +0);
            
        } else{
            switch(random){
        
            case LETTER_A:
                currentTask = LETTER_A;
                task.innerHTML = CLOSER_A.charAt(Math.floor((Math.random() *13) +0));
                break;
                
            case LETTER_Z:
                currentTask = LETTER_Z;
                task.innerHTML = CLOSER_Z.charAt(Math.floor((Math.random() *13) +0));
                break;
            
            default:
                break;
                
            }
        }
        onNewTask();
    }
    
    function startSpiel() {
        if(!gameRunning){
            gameRunning = true;
            getTask();
            countdown(TIME);
        }
    }
    
    function updateTimer(time){
        var timeView = "",
            minutes,
            seconds;
        minutes = Math.floor(time/60);
        seconds = time - minutes*60;
        
        if(seconds < 10) {
            timeView = minutes + ":0" + seconds;
        } else{
            timeView = minutes + ":" + seconds;
        }
        
        timer.innerHTML = timeView;
    }
    
    function countdown(time) {
        var timeout;
        if(gameRunning){
            updateTimer(time);
            if(time > 0) {
                timeout = window.setTimeout(countdown, 1000, time-1);
            } else{
                gameRunning = false;
            }
        }
    }
    
    function start() {
        spielAlphabetZahlen.classList.remove("hidden");
    }
    
    function zurückZumMenü() {
        if(gameRunning){
            gameRunning = false;
        }
        
        updateTimer(TIME);
        spielAlphabetZahlen.classList.add("hidden");
        that.notifyAll("zumMenü");
    }
    
    document.onkeydown = function(event) {
        if(gameRunning){
            switch(currentTask) {
            case LETTER_A:
                if(event.keyCode == KEYCODE_N){
                    onRightAnswer();
                } else {
                    onWrongAnswer();
                }
                break;
                    
            case LETTER_Z:
                if(event.keyCode == KEYCODE_M){
                    onRightAnswer();
                } else {
                    onWrongAnswer();
                }
                break;
                    
            case NUMBER:
                if(task.innerHTML < 5) {
                    if(event.keyCode == KEYCODE_Y){
                        onRightAnswer();
                    } else {
                        onWrongAnswer();
                    }
                } else {
                    if(event.keyCode == KEYCODE_X) {
                        onRightAnswer();
                    } else {
                        onWrongAnswer();
                    }
                }
                break;
                
            default:
                break;
            }
        }
    };
    
    
    
    function init() {
        spielAlphabetZahlen = document.getElementById("spielAlphabetZahlen");
        timer = document.getElementById("timerAlphabetZahlen");
        startButton = document.getElementById("startSpielAlphabetZahlen");
        startButton.addEventListener("click",startSpiel);
        task = document.getElementById("aufgabeAlphabetZahlen");
        zurückButton = document.getElementById("zurückAlphabetZahlen");
        zurückButton.addEventListener("click",zurückZumMenü);
        updateTimer(TIME);
    }
    
    
    that.init = init;
    that.start = start;
    return that;
}());