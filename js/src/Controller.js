var TrainYourBrain = TrainYourBrain || {};
TrainYourBrain.Controller = (function (){
    "use strict";
    /* eslint-env browser */
    var that = new EventPublisher(),
        buttonStartAlphabetZahlen,
        buttonStartClickIt,
        buttonStartKopfrechnen,
        buttonStartSchwarzeQuadrate,
        buttonStartWörterMerken,
        buttonStartWortpaare,
        hauptmenü;
    
    function onAlphabetZahlenClicked() {
        hauptmenü.classList.add("hidden");
        that.notifyAll("alphabetZahlenStarted");
    }
    
    function onClickItClicked() {
        that.notifyAll("clickItStarted");
    }
    
    function onKopfrechnenClicked() {
        hauptmenü.classList.add("hidden");
        that.notifyAll("kopfrechnenStarted");
    }
    
    function onSchwarzeQuadrateClicked() {
        that.notifyAll("schwarzeQuadrateStarted");
    }
    
    function onWörterMerkenClicked() {
        hauptmenü.classList.add("hidden");
        that.notifyAll("wörterMerkenStarted");
    }
    
    function onWortpaareClicked() {
        that.notifyAll("wortpaareStarted");
    }
    
    function restore() {
        hauptmenü.classList.remove("hidden");
    }
    
    function init() {
        buttonStartAlphabetZahlen = document.getElementById("startAlphabetZahlen");
        buttonStartClickIt = document.getElementById("startClickIt");
        buttonStartKopfrechnen = document.getElementById("startKopfrechnen");
        buttonStartSchwarzeQuadrate = document.getElementById("startSchwarzeQuadrate");
        buttonStartWörterMerken = document.getElementById("startWörterMerken");
        buttonStartWortpaare = document.getElementById("startWortpaare");
        
        buttonStartAlphabetZahlen.addEventListener("click", onAlphabetZahlenClicked);
        buttonStartClickIt.addEventListener("click", onClickItClicked);
        buttonStartKopfrechnen.addEventListener("click", onKopfrechnenClicked);
        buttonStartSchwarzeQuadrate.addEventListener("click", onSchwarzeQuadrateClicked);
        buttonStartWörterMerken.addEventListener("click", onWörterMerkenClicked);
        buttonStartWortpaare.addEventListener("click", onWortpaareClicked);
        
        hauptmenü = document.getElementById("Hauptmenü");
    }
    
    that.init = init;
    that.restore = restore;
    return that;
}());