var TrainYourBrain = TrainYourBrain || {};
TrainYourBrain = (function (){
    "use strict";
    /* eslint-env browser */
    var that = {},
        controller,
        kopfrechnen,
        alphabetZahlen,
        wörterMerken;
    
    function init() {
        controller = TrainYourBrain.Controller;
        controller.init();
        
        controller.addEventListener("wörterMerkenStarted", wörterMerkenStart);
        wörterMerken = TrainYourBrain.WörterMerken;
        wörterMerken.init();
        wörterMerken.addEventListener("zumMenü", zurückZumMenü);
        
        controller.addEventListener("kopfrechnenStarted", kopfrechnenStart);
        kopfrechnen = TrainYourBrain.Kopfrechnen;
        kopfrechnen.init();
        kopfrechnen.addEventListener("zumMenü", zurückZumMenü);
        
        controller.addEventListener("alphabetZahlenStarted", alphabetZahlenStart);
        alphabetZahlen = TrainYourBrain.AlphabetundZahlen;
        alphabetZahlen.init();
        alphabetZahlen.addEventListener("zumMenü", zurückZumMenü);
        
    }
    
    function wörterMerkenStart(){
        
        wörterMerken.start();
        
    }
    
    function kopfrechnenStart(){
        
        kopfrechnen.start();
        
    }
    
    function alphabetZahlenStart() {
        alphabetZahlen.start();
    }
    
    function zurückZumMenü(){
        controller.restore();
    }
    
    that.init = init;
    return that;
}());