var TrainYourBrain = TrainYourBrain || {};
TrainYourBrain.WörterMerken = (function (){
    "use strict";
    /* eslint-env browser */
    var that = new EventPublisher(),
        bodyWM,
        spielWM,
        timerWM,
        startWM,
        aufgabeWM,
        lösungWM,
        lösungsfeldWM,
        eingabeWM,
        bestätigenWM,
        beendenWM,
        zurückWM,
        gameRunning = false,
        String ="",
        lösungString = "";
    
    const TIME = 5;
    
    var wörter = new Array ("Hund", "Pferd", "Esel", "Schnur", "Gründung", "Beratung", "Büro", "Nachwuchs", "Telefonie", "Internet", "Gründerzentrum", "Stundenplan", "Förderung", "Steckdose", "Entwicklung", "Software", "Europa", "Mindestdauer", "Information", "Arbeitsplatz", "Kaugummi", "Professor", "Dilettant", "Apartheid", "Antipathie", "Epidemie", "Entgelt", "Gecko", "Chameleon", "Geißel", "Gingko", "Gelatine", "Hobby", "Karussell", "Komitee", "Kommilitone", "Koryphäe", "Kumulation", "Lizenz", "Lärche", "Laptop", "Misere", "Obolus", "Paket", "Paneel", "Portfolio", "Prozess", "Rhythmus", "Rhetorik", "Säbelzahntiger", "Schmalz", "Schloss", "Satellit", "Silhouette", "Souterrain", "Strophe", "Wolf", "Trilogie", "Vegetarier", "Spiegelbild", "Wermut", "Widerspruch", "Voraussetzung", "Triumph", "Toleranz", "Terasse", "Symmetrie", "Standard", "Stracciatella", "Spaghetti", "Silvester", "Resümee", "Altenheim", "Ball", "Auto", "Buch", "Demenz", "Dach", "Eisenbahn", "Engel", "Freiheit", "Gehirn", "Igel", "Insel", "Kammer", "Keller", "Kaktus", "Kugel", "Obst", "Papier", "Quark", "Kröte", "Tasche", "Rettungswagen", "Umwelt", "Verkehr", "Straußenvogel", "Zahn", "Wasser", "Yogalehrer");
    
        
    
    function init() {
        
        bodyWM = document.getElementById("wörterMerkenBody");
        spielWM = document.getElementById("aufgabeWörterMerken");
        timerWM = document.getElementById("timerWörterMerken");
        updateTimer(TIME);
        startWM = document.getElementById("startSpielWörterMerken");
        startWM.addEventListener("click",startSpiel);
        aufgabeWM = document.getElementById("merkeWörter");
        lösungWM = document.getElementById("lösungWörterMerken");
        lösungsfeldWM = document.getElementById("lösungsfeldWörterMerken");
        eingabeWM = document.getElementById("eingabeWörterMerken");
        bestätigenWM = document.getElementById("wörterMerkenBestätigen");
        bestätigenWM.addEventListener("click", bestätigeEingabe);
        beendenWM = document.getElementById("spielBeendenWörterMerken");
        zurückWM = document.getElementById("zurückWörterMerken");
        zurückWM.addEventListener("click", zurückZumMenü);
       
    }
    
    function start(){
        
        bodyWM.classList.remove("hidden"); 
        
    }
    
    
    function startSpiel() {
        if(!gameRunning){
            aufgabeWM.innerHTML = getTask();
            gameRunning = true;
            countdown(TIME);
        }
    }
    
    function updateTimer(time){
        var timeView = "",
            minutes,
            seconds;
        minutes = Math.floor(time/60);
        seconds = time - minutes*60;
        
        if(seconds < 10) {
            timeView = minutes + ":0" + seconds;
        } else{
            timeView = minutes + ":" + seconds;
        }
        
        timerWM.innerHTML = timeView;
    }
    
    
    function countdown(time) {
        var timeout;
        if(gameRunning){
            updateTimer(time);
            if(time > 0) {
                timeout = window.setTimeout(countdown, 1000, time-1);
            } else{
                spielWM.classList.add("hidden");
                lösungWM.classList.remove("hidden");
                
            }
        }
    }
    

    
    function bestätigeEingabe(){
        
        if(String.indexOf(eingabeWM.value) > -1){
            
            lösungString = lösungString + " " + eingabeWM.value;
            lösungsfeldWM.innerHTML = lösungString;
            eingabeWM.value = "";
            console.log("richtig");
            
        } else {
            
            eingabeWM.value = "";
            console.log("falsch");
            
        }
        
    }
    
    function zurückZumMenü() {
        if(gameRunning){
            gameRunning = false;
        }
        
        updateTimer(TIME);
        spielWM.classList.add("hidden");
        that.notifyAll("zumMenü");
    }
    
    function getTask(){
        var i,
            random;
        
        for(i = 0; i < 15; i++){
            
            random = wörter[Math.floor(Math.random() * wörter.length)];
            String = String + " " + random;
        
        }
        
        return String;
        
    }
    
    that.init = init;
    that.start = start;
    return that;
}());